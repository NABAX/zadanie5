import java.util.Scanner;


public class Calculator {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int num1 = getInt();
        char operation = getOperation();
        int num2 = getInt();
        int result = calc(num1,num2, operation);
        System.out.println("Результат операции: "+result);
        }




    private static int getInt(){
        System.out.println("Введите число:");
        int num;
        if(scanner.hasNextInt()){
            num = scanner.nextInt();
        } else {

            num = getInt();
        }
        return num;
    }

    private static char getOperation(){
        System.out.println("Введите операцию:");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else {

            operation = getOperation();
        }
        return operation;
    }

    private static int calc(int num1, int num2, char operation){
        int result;
        switch (operation){
            case '+':
                result = num1+num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '*':
                result = num1*num2;
                break;
            case '/':
                if ( num2 == 0 ) {
                        throw new ArithmeticException ( "Деление на ноль!" );
                }
                try {
                    num2 = 0;
                }
                catch (ArithmeticException e) {

                    System.out.println("Деление на ноль!");

                }

                result = num1/num2;
                break;
            case '^':
                if (num2 == 0) {
                    result = 1;
                } else {
                    result = (int) Math.pow(num1, num2);}
                break;
            default:
                result = calc(num1, num2, getOperation());
        }
        return result;
    }
}

